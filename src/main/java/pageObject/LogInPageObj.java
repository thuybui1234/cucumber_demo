package pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogInPageObj {
    public static String USER_NAME = "xpath:=//input[@data-test='username']";
    public static String PASS_WORD = "xpath:=//input[@data-test='password']";
    public static String LOG_IN = "xpath:=//input[@value='Login']";

}
