package pageAction;

import org.openqa.selenium.WebDriver;
import pageObject.LogInPageObj;
import webKeyword.WebKeyWords;

public class LogInAction extends WebKeyWords {

    public LogInAction(WebDriver driver){
        super(driver);
    }

    public LogInAction inputUserInfo(String username, String password){
        sendKeyToElement(LogInPageObj.USER_NAME, username);
        sendKeyToElement(LogInPageObj.PASS_WORD, password);
        return this;
    }

    public LogInAction clickOnLogInButton(){
        clickToElement(LogInPageObj.LOG_IN);
        return this;
    }
    public LogInAction openBrowser(){
        getUrl("https://www.saucedemo.com/");
        return this;
    }


}
