package pageAction;

import common.CommonUltils;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObject.SwagLabsPageObj;
import webKeyword.WebKeyWords;

public class SwagLabsPageAction extends WebKeyWords {
    public SwagLabsPageAction(WebDriver driver) {
        super(driver);
    }

    public SwagLabsPageAction clickAddToCart() {
        int number = 0;
        for (int i = 0; i < 2; i++) {
            int number1 = CommonUltils.getRandomValue(6);
            while (number == number1) {
                number1 = CommonUltils.getRandomValue(6);
            }
            number = number1;
            waitForElementVisible(SwagLabsPageObj.CLICK_ADD_TO_CART, String.valueOf(number));
            if (getTextElement(SwagLabsPageObj.CLICK_ADD_TO_CART, String.valueOf(number)).equals("Add to cart")) {
                clickToElementByJavascript(SwagLabsPageObj.CLICK_ADD_TO_CART, String.valueOf(number));
                verifyItemStatusChanged(number);
            }
        }
        return this;
    }

    public SwagLabsPageAction verifyItemStatusChanged(int number) {
        waitForElementVisible(SwagLabsPageObj.REMOVE_BUTTON, String.valueOf(number));
        Assert.assertTrue(isControlDisplayed(SwagLabsPageObj.REMOVE_BUTTON, String.valueOf(number)));
        return this;
    }

    public SwagLabsPageAction verifyQuantityItemsIsCorrect(String quantity) {
        String number = getTextElement(SwagLabsPageObj.QUANTITY_ITEMS);
        Assert.assertEquals(number, quantity);
        return this;
    }

    public SwagLabsPageAction clickOnCart() {
        clickToElementByAction(SwagLabsPageObj.CART_ITEMS);
        return this;
    }

    public SwagLabsPageAction verifyRedirectToInventory() {
        String url = getCurrentPageUrl();
        waitForPageLoaded();
        Assert.assertEquals(url, "https://www.saucedemo.com/inventory.html");
        return this;
    }
    public SwagLabsPageAction openSwagLabPage(String url) {
        openPageUrl(url);
        return this;
    }
}
