package webKeyword;


import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class WebKeyWords {
    private static int defaultTimeout;
    private static int shortTimeout;
    WebDriverWait waitExplicit;
    WebElement element;
    By byLocator;
    Actions action;
    JavascriptExecutor javascriptExecutor;
    public WebDriver driver;


    public WebKeyWords(WebDriver driver){
        this.driver = driver;
        waitExplicit = new WebDriverWait(driver, 30);
    }

    public void clickToElement(String locator, int...timeout){
        try {
        WebElement webElement = getElement(locator);
        if(webElement.isEnabled()){
            webElement.click();
        }
        } catch (Exception e) {
        }
    }

    public void clickToElement(WebElement webElement, int...timeout){
        try {
            if(webElement.isEnabled()){
                webElement.click();
            }
        } catch (Exception e) {
        }
    }

    private By getBy(String anyLocator){
            String locator[] = anyLocator.split(":=");
            final By by;
            switch(locator[0]){
                case "xpath":
                    by = By.xpath(locator[1]);
                    break;
                case "css":
                    by = By.cssSelector(locator[1]);
                    break;
                case "className":
                    by = By.className(locator[1]);
                    break;
                case "id":
                    by = By.id(locator[1]);
                    break;
                case "name":
                    by = By.name(locator[1]);
                    break;
                default:
                    throw new RuntimeException("Invalid locator!!!");
            }
            return by;
    }

    private By getByLocator(String anyLocator, String... params){
        return params.length==0?getBy(anyLocator):getBy(String.format(anyLocator, (Object[]) params));
    }

    public WebElement getElement(String anyLocator, int...timeout){
        int waitTime = timeout.length > 0 ? timeout[0] : defaultTimeout;
        int poolingTime = waitTime / 10;
        final By by = getByLocator(anyLocator);
        WebElement we = null;
        try {
            Wait<WebDriver> wait = new FluentWait<WebDriver>(this.driver).withTimeout(Duration.ofSeconds(waitTime))
                    .pollingEvery(Duration.ofSeconds(poolingTime)).ignoring(NoSuchElementException.class);
            we = wait.until(new Function<WebDriver, WebElement>() {

                public WebElement apply(WebDriver driver) {
                    // TODO Auto-generated method stub
                    return driver.findElement(by);
                }
            });
            if (we != null) {
                return we;
            }
        } catch (TimeoutException e) {
        }
        return null;
    }

    public WebElement waitForElementVisible(String locator, String... values) {
        this.waitExplicit = new WebDriverWait(this.driver, defaultTimeout);
        this.byLocator = getByLocator(locator, values);

        try {
            return this.waitExplicit.until(ExpectedConditions.visibilityOfElementLocated(this.byLocator));
        } catch (Exception var4) {
            System.err.println("================================== Element not visible===================================");
            System.err.println(var4.getMessage() + "\n");
            return null;
        }
    }
    public WebElement waitForElementVisible(WebElement element){

        this.waitExplicit = new WebDriverWait(this.driver, defaultTimeout);
        try {
            element = this.waitExplicit.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception var3) {
            System.err.println("================================== Element not visible===================================");
            System.err.println(var3.getMessage() + "\n");
        }

        return element;
    }
    public String getTextElement(String locator, String... values) {
        this.element = this.waitForElementVisible(locator, values);
        return this.element.getText();
    }
    public String getTextElement(WebElement element) {
        element = this.waitForElementVisible(element);
        return element.getText();
    }
    public boolean isControlDisplayed(String locator, String... values) {
        this.element = this.driver.findElement(getByLocator(locator, values));
        return this.element.isDisplayed();
    }
    public void overrideGlobalTimeout(int timeOut) {
        this.driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
    }
    public void waitForElementInvisible(String locator, long timeout, String... values) {
        this.overrideGlobalTimeout(shortTimeout);
        this.waitExplicit = new WebDriverWait(this.driver, timeout);
        this.byLocator = getByLocator(locator, values);

        try {
            this.waitExplicit.until(ExpectedConditions.invisibilityOfElementLocated(this.byLocator));
        } catch (Exception var6) {
        }

        this.overrideGlobalTimeout(defaultTimeout);
    }

    public void waitForElementInvisible(WebElement element) {
        this.overrideGlobalTimeout(shortTimeout);
        this.waitExplicit = new WebDriverWait(this.driver, defaultTimeout);

        try {
            this.waitExplicit.until(ExpectedConditions.invisibilityOf(element));
        } catch (Exception var3) {
            System.err.println("================================== Element not invisible===================================");
            System.err.println(var3.getMessage() + "\n");
        }

        this.overrideGlobalTimeout(defaultTimeout);
    }
    public void clickToElementByAction(WebElement element) {
        this.javascriptExecutor.executeScript("arguments[0].scrollIntoViewIfNeeded(true);", new Object[]{element});
        this.action = new Actions(this.driver);
        this.waitForElementVisible(element);
        this.action.click(element).perform();
    }
    public void clickToElementByAction(String locator, String... values) {
        this.element = this.waitForElementVisible(locator, values);
        this.javascriptExecutor.executeScript("arguments[0].scrollIntoViewIfNeeded(true);", new Object[]{element});
        this.action = new Actions(this.driver);
        this.waitForElementVisible(element);
        this.action.click(element).perform();
    }
    public void clickToElementByJavascript(String locator, String... values) {
        locator = String.format(locator, (Object[])values);
        this.javascriptExecutor = (JavascriptExecutor)this.driver;
        this.element = this.driver.findElement(getByLocator(locator));
        this.javascriptExecutor.executeScript("arguments[0].click();", new Object[]{this.element});
    }
    public void clickToElementByJavascript(WebElement element) {
        this.javascriptExecutor = (JavascriptExecutor)this.driver;
        this.javascriptExecutor.executeScript("arguments[0].click();", new Object[]{element});
    }

    public WebElement waitForElementClickable(WebElement element){
        this.waitExplicit = new WebDriverWait(this.driver, defaultTimeout);
        return  (WebElement)this.waitExplicit.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void clearTextElement(WebElement element){
        element = this.waitForElementVisible(element);
        element.clear();
    }
    public void sendKeyToElement(WebElement element, String value) {
        element = this.waitForElementVisible(element);
        element.sendKeys(new CharSequence[]{value});
    }

    public void sendKeyToElement(String locator, String sendKeyValue, String... values) {
        this.element = this.waitForElementVisible(locator, values);
        this.element.sendKeys(new CharSequence[]{sendKeyValue});
    }

    public void dragAndDrop(String sourceLocator, String targetLocator) {
        WebElement sourceElement = this.driver.findElement(By.xpath(sourceLocator));
        WebElement targetElement = this.driver.findElement(By.xpath(targetLocator));
        this.action = new Actions(this.driver);
        this.action.dragAndDrop(sourceElement, targetElement).build().perform();
    }
    public void dragAndDrop(WebElement sourceElement, WebElement targetElement) {
        this.action = new Actions(this.driver);
        this.action.dragAndDrop(sourceElement, targetElement).build().perform();
    }

    public void waitForPageLoaded() {
        ExpectedCondition expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState", new Object[0]).toString().equals("complete");
            }
        };

        try {
            Thread.sleep(1000L);
            WebDriverWait wait = new WebDriverWait(this.driver, 30L);
            wait.until(expectation);
        } catch (Throwable var3) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }

    }
    public void clickToElementByJavascript(String locator) {
        this.javascriptExecutor = (JavascriptExecutor) this.driver;
        this.element = this.driver.findElement(getByLocator(locator));
        this.javascriptExecutor.executeScript("arguments[0].click();", new Object[]{this.element});
    }

    public void sleepTimeInMilSecond(int milSecond) {
        try {
            Thread.sleep((long)milSecond);
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }

    }

    public void scrollToElement(Object locatorOrElement, String... values) {
        if (locatorOrElement instanceof WebElement) {
            this.element = (WebElement)locatorOrElement;
        } else {
            String locator = String.format((String)locatorOrElement, (Object[])values);
            this.element = this.driver.findElement(getByLocator(locator));
        }

        ((JavascriptExecutor)this.driver).executeScript("arguments[0].scrollIntoView(true);", new Object[]{this.element});
        this.sleepTimeInMilSecond(500);
    }

    public String getElementValidationMessage(WebElement element ) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) this.driver;
        return (String) jsExecutor.executeScript("return arguments[0].validationMessage;", element);
    }


    public String getCurrentPageUrl() {
        return this.driver.getCurrentUrl();
    }

    public void getUrl(String url) {
        this.driver.get(url);
    }
    public void openPageUrl(String url) {
        this.driver.get(url);
    }

}
