package demo.cucumber.stepDefinition;

import common.Constains;
import driver.DriverManager;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageAction.SwagLabsPageAction;

public class SwagLabsPageStep {
    SwagLabsPageAction swagLabsPageAction;
    public SwagLabsPageStep(){
        swagLabsPageAction = new SwagLabsPageAction(DriverManager.getDriver());
    }

    @When("get random two item add to cart")
    public void addItemToCart(){
        swagLabsPageAction.clickAddToCart();
    }

    @Then("random two items is added to cart")
    public void itemsCorrect(){
        swagLabsPageAction.verifyQuantityItemsIsCorrect("2");
    }
    @When("open browser swag lab page")
    public void openBrowser(){
        swagLabsPageAction.openSwagLabPage(Constains.SWAG_LAB_PAGE_URL);
    }

}
