package pageObject;

public class SwagLabsPageObj {

    public static String QUANTITY_ITEMS = "xpath:=//*[@class='shopping_cart_link']//span";
    public static String CART_ITEMS = "xpath:=//*[@class='shopping_cart_container']";
    public static final String CLICK_ADD_TO_CART = "xpath:=//div[@class='inventory_item'][%s]//button[text()='Add to cart']";
    public static final String REMOVE_BUTTON = "xpath:=//div[@class='inventory_item'][%s]//button[text()='Remove']";
}
