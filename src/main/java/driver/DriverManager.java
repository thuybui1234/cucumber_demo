package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DriverManager {
    private static WebDriver driver;
    private static int LONG_TIMEOUT = 30;

    public static WebDriver getDriver(){
        return driver;
    }

    public synchronized static WebDriver createDriver(String browser, String url){
        switch (browser){
            case "chrome":
                WebDriverManager.chromedriver().setup();
                String language = "en_GB";
                ChromeOptions option = new ChromeOptions();
                Map<String, Object> chrome_prefs = new HashMap<>();
                chrome_prefs.put("intl.accept_languages", language);
                option.addArguments("--incognito");
                option.addArguments("--disable-extensions");
                option.addArguments("--disable-infobars");
                option.setExperimentalOption("prefs", chrome_prefs);
                driver = new ChromeDriver();
                break;
            case "firefox":
                break;
        }
        driver.get(url);
        getDriver().manage().timeouts().implicitlyWait(getLONG_TIMEOUT(), TimeUnit.SECONDS);
        getDriver().manage().window().maximize();
        return getDriver();

    }
    private static int getLONG_TIMEOUT() {
        return LONG_TIMEOUT;
    }

    public static void closeBrowserAndDriver(){
        getDriver().manage().deleteAllCookies();
        getDriver().quit();

    }

}
