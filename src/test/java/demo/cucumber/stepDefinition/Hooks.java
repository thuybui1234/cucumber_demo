package demo.cucumber.stepDefinition;

import driver.DriverManager;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks {

    @Before
    public static void openBrowser(){
        DriverManager.createDriver("chrome", "https://www.saucedemo.com/");
    }

    @After
    public static void quitBrowser(){
        DriverManager.closeBrowserAndDriver();
    }



}
