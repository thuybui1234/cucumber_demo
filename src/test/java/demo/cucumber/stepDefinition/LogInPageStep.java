package demo.cucumber.stepDefinition;


import driver.DriverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageAction.LogInAction;

public class LogInPageStep {
    LogInAction logInAction;

    public LogInPageStep(){
        logInAction = new LogInAction(DriverManager.getDriver());
    }


    @Given("open browser")
    public void openBrowser(){
        logInAction.openBrowser();
    }

    @When("fill username and password")
    public void inputUserNameAndPassword(){
        logInAction.inputUserInfo("standard_user", "secret_sauce");
    }

    @When("log in button")
    public void clickOnLogInButton(){
        logInAction.clickOnLogInButton();
    }
}
