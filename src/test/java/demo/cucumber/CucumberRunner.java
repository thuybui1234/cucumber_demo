package demo.cucumber;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;


@CucumberOptions(
        features = "src/test/resources/feature",
        glue = "demo.cucumber.stepDefinition",
        plugin = {"pretty", "html:target/htmlreports.html"},
        tags = "@add_item_to_cart"
)


public class CucumberRunner extends AbstractTestNGCucumberTests {

}
